﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragComponent : MonoBehaviour
{
    private Plane _dragPlane;
    private Vector3 _offset;
    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    public void OnMouseDown()
    {
        _dragPlane = new Plane(_camera.transform.forward, transform.position);
        var camRay = _camera.ScreenPointToRay(Input.mousePosition);

        _dragPlane.Raycast(camRay, out var planeDist);
        _offset = transform.position - camRay.GetPoint(planeDist);
    }

    private void OnMouseDrag()
    {
        var camRay = _camera.ScreenPointToRay(Input.mousePosition);

        _dragPlane.Raycast(camRay, out var planeDist);
        transform.position = camRay.GetPoint(planeDist) + _offset;
    }
}
